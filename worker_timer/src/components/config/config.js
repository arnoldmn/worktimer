const STORAGE_ACTIVITY_KEY = '@activities';
const APP_TIME_STORAGE_KEY = '@appStateChangedTimestamp';
const TIME_STORAGE_KEY = '@time';
const isPAUSED_STORAGE_KEY = '@isPaused';

export default {
    STORAGE_ACTIVITY_KEY,
    APP_TIME_STORAGE_KEY,
    TIME_STORAGE_KEY,
    isPAUSED_STORAGE_KEY
};