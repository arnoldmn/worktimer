import { StyleSheet } from 'react-native';

const HistoryViewStyles = StyleSheet.create({
    historyItemcontainer: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#EAEAEA',
        padding: 12,
        margin: 5,
        height: 55,
    },

    historyItemNameText: {
        fontSize: 18,
    },

    historyItemDetailsText: {
        fontSize: 14,
    },

    historyItemDetailsContainer: {
        flex: 2,
        alignItems: 'flex-end',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    
    historyItemHeaderText: {
        fontSize: 30,
        margin: 20,
        textAlign: 'center',
        textTransform: 'capitalize',
    }

});

export default HistoryViewStyles;