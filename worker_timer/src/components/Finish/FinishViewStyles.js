import {StyleSheet} from 'react-native';

const FinishViewStyles = StyleSheet.create({
    mainHeader: {
        fontSize: 40,
        textAlign: 'center',
        flex: 2,
    },

    timerSubHeader: {
        fontSize: 40,
        textAlign: 'center',
        flex: 2,
    },

    activityNameLabel: {
        fontSize: 16,
    },

    activityNameInput: {
        borderRadius: 10,
        borderColor: "#000000",
        borderWidth: 3,
        height: 44,
        marginTop: 7,
        padding: 10,
    },
     actionButtonContainer: {
        marginTop: 65,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});

export default FinishViewStyles;