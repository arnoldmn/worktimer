import React from 'react';
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';

const ActiionButtonStyles = StyleSheet.create({
    touchableStyle: {
        width: 134,
        height: 44,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },

    captionStyle: {
        fontSize: 24,
        textTransform: 'lowercase',
    }
});

const ActionButton = ({ onPress, label, textColor, backgroundColor }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[
                ActiionButtonStyles.touchableStyle, 
            {
                backgroundColor,
            }
            ]
            }
        >
            <Text style={[{color: textColor}, ActiionButtonStyles.captionStyle ]}>{label}</Text>
        </TouchableOpacity>
    )
};

export default ActionButton;