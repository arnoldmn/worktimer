import React from 'react'
import { Animated, Text, TouchableOpacity } from "react-native";
import i18n from "../../i18n/i18n";
import HomeViewStyles from "../Home/HomeViewStyles";
import moment from 'moment'

const StopWatchButton = ({
    time,
    startOnPressAction,
    timerOnPressAction,
    paused,
}) => {
    const timerOpacity = new Animated.Value(1);
    const BlinkDelay = 500;
    const blinker = (toValue) => {
        if (paused) {
            Animated.timing(timerOpacity, {
                toValue,
                duration: BlinkDelay,
                useNativeDriver: true,
            }).start(() => {
                blinker( toValue === 1 ? 0 : 1);
            });
        } else {
            Animated.timing(timerOpacity, {
                toValue: 1,
                duration: BlinkDelay,
                useNativeDriver: false
            }).start();
        }
    };

    blinker(0);

    if (time > 0) {
        return (
            <TouchableOpacity
                style={HomeViewStyles.mainActionButton}
                onPress={timerOnPressAction}
            >
                <Animated.View style={{ opacity: timerOpacity }}>
                    <Text style={HomeViewStyles.mainActionButtonText}>
                        {moment.utc(time).format('HH:mm:ss')}
                    </Text>
                    <Text style={[
                        HomeViewStyles.mainActionButtonText,
                        HomeViewStyles.mainActionButtonPauseText
                    ]}>

                    </Text>
                </Animated.View>
            </TouchableOpacity>
        )
    }
    return (
        <TouchableOpacity
            style={HomeViewStyles.mainActionButton}
            onPress={startOnPressAction}
        >
            <Text style={HomeViewStyles.mainActionButtonText}>{i18n.STOP_WATCH_START.START}</Text>
        </TouchableOpacity>
    )
}

export default StopWatchButton;