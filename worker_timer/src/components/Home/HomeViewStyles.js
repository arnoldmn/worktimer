import { StyleSheet } from 'react-native'

const HomeViewStyles = StyleSheet.create({
    homeViewcontainer: {
        alignItems: 'center',
    },
    welcomeHeader: {
        marginTop: 50,
        textAlign: 'center',
        fontSize: 40,
        color: '#000000'
    },

    mainActionButton: {
        width: 284,
        height: 284,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderRadius: 142,
        borderWidth: 4
    },

    mainActionButtonText: {
        fontSize: 70,
        color: '#000000',
        fontWeight: 'bold'
    },

    finishButtonText: {
        fontSize: 60,
        // fontWeight: 'bold',
        color: '#000000',
        marginBottom: 10,
        borderColor: '#000000',
        borderWidth: 3,
        borderRadius: 10,
    },
    buttonsContainer: {
        alignItems: 'center', 
        justifyContent: 'space-between'
    },
    mainActionButtonPauseText: {
        fontSize: 24,

    }

})

export default HomeViewStyles;