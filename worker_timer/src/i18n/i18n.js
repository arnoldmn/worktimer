const i18n = {
    TIME_FORMAT: 'HH:mm:ss',
    CANCEL: 'CANCEL',
    SAVE: 'SAVE',
    DATE_FORMAT: 'DD MMM YYYY',
    SAVED_ACTIVITIES: 'Saved activities',

    HOME: {
        WELCOME_HEADER: 'Good Morning!',
        START: 'START',
        FINISH: 'FINISH'
    },

    STOP_WATCH_START: {
        START: 'START'
    },

    FINISH: {
        MAIN_HEADER: 'You just worked for',
        ACTIVITY_NAME_LABEL: 'Activity name'

    }
};

export default i18n;