import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, Dimensions, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import HomeView from './src/components/Home/HomeView';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import FinishView from './src/components/Finish/FinishView';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import HistoryView from './src/components/History/HistoryView';

const windowHeight = Dimensions.get('window').height;

const HomeNavigator = createStackNavigator({
  Home: {
    screen: HomeView,
  },

  Finish: {
    screen: FinishView,
  },
},
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);

function App() {
  return (
    <SafeAreaView style={styles.container}>
      <HomeView />
    </SafeAreaView>
  );
}


const BottomTabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeNavigator,
    showIcon: true,
  },

  History: {
    screen: HistoryView,
  }
}, {
  tabBarOptions: {
    activeTintColor: '#FFFFFF',
    inactiveTintColor: '#A3A3A3',
    showIcon: true,
    height: 34,
    labelStyle: {
      fontSize: 15,

    },
    style: {
      position: 'relative',
      backgroundColor: '#000000',
      borderColor: 'rgba(140, 140, 140, 0.8)',
      borderTopWidth: 1,
    }
  },
  
},
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',

  },
});

export default createAppContainer(BottomTabNavigator, App);
